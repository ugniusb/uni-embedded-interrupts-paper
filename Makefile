TARGETS=embedded-interrupts-fp plan
BUILD_DIR=build
OUTPUT_DIR=pdf

INCLS_DIR=incls
INCLS=$(wildcard $(INCLS_DIR)/*) $(wildcard spec/*)
IMG_DIR=img
PNGS=$(wildcard $(IMG_DIR)/*.png)
BIB_DIR=bib
BIBS=$(wildcard $(BIB_DIR)/*.bib)
BIB_B=$(BUILD_DIR)/bibliografija.bib
PDFS=$(addprefix $(OUTPUT_DIR)/,$(addsuffix .pdf, $(TARGETS)))

AUXILIARIES=VUMIFPSbakalaurinis.cls LTPlius.sty Makefile $(INCLS)

LATEX_FLAGS=-halt-on-error
# --shell-escape # shell-escape for minted or dot2texi
#   xelatex
# LATEX=xelatex
# FAST_MODE=--no-pdf
#   lualatex
LATEX=lualatex
LATEX_FLAGS:=$(LATEX_FLAGS)
FAST_MODE=--draftmode

BIBER_FLAGS=-q

.PHONY: all view clean rebuild prepare


all: prepare $(PDFS)


view: $(PDFS)
	gio open $<


clean:
	rm -rf $(OUTPUT_DIR)
	rm -rf $(BUILD_DIR)


rebuild: clean all


prepare:
	# remove any lingering state
	rm -rf $(BUILD_DIR)


$(OUTPUT_DIR)/%.pdf: $(BUILD_DIR)/%.pdf
	@mkdir -p $(OUTPUT_DIR)
	cp $< $@


$(BIB_B): $(BIBS)
	@mkdir -p $(BUILD_DIR)
	cat $^ >$@

$(BUILD_DIR)/%.pdf: %.tex $(BUILD_DIR)/%.bbl $(BUILD_DIR)/%.aux $(AUXILIARIES)
	@mkdir -p $(BUILD_DIR)
	$(LATEX) $(LATEX_FLAGS) -interaction=errorstopmode \
		--output-directory=$(BUILD_DIR) $<

$(BUILD_DIR)/%.aux: %.tex $(AUXILIARIES)
	@mkdir -p $(BUILD_DIR)
	$(LATEX) $(LATEX_FLAGS) -interaction=errorstopmode $(FAST_MODE) \
		--output-directory=$(BUILD_DIR) $<

# duplication for no reason except "undefined references" WTF
$(BUILD_DIR)/%.bcf: %.tex $(AUXILIARIES)
	@mkdir -p $(BUILD_DIR)
	$(LATEX) $(LATEX_FLAGS) -interaction=errorstopmode $(FAST_MODE) \
		--output-directory=$(BUILD_DIR) $<

$(BUILD_DIR)/%.bbl: %.tex $(BIB_B) $(filter-out $(wildcard $(BUILD_DIR)/%.bcf), $(BUILD_DIR)/%.bcf) $(AUXILIARIES)
	@mkdir -p $(BUILD_DIR)
	biber $(BIBER_FLAGS) --output-directory $(BUILD_DIR) $(basename $(<F))
