---- MODULE Maybe ----
(*
Simple "nullable" fields.
*)
LOCAL INSTANCE Sequences

Just(x) == << x >>
None == << >>

Maybe(t) == UNION { {None}, {Just(x): x \in t} }

IsJust(mx) == Len(mx) = 1
IsNone(mx) == Len(mx) = 0
GetJust(mx) == Head(mx)
==============================================================================
