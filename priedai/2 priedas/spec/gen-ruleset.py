#!/usr/bin/env python3.7
import itertools
import random


def mk_seq(*args):
    strs = map(str, args)
    return f"<< {', '.join(strs)} >>"


def mk_set(*args):
    strs = map(str, args)
    return f"{{ {', '.join(strs)} }}"


def mk_str(x):
    return f'"{x}"'


def mk_atomic_name(x):
    return mk_str(f"a_{x}")


def mk_nonatomic_name(x):
    return mk_str(f"n_{x}")


def mk_rule(reads, writes, duration):
    return f"Rule({mk_seq(*reads)}, {mk_seq(*writes)}, {duration})"


def subsets(xs):
    return map(set,
               itertools.chain(*(
                   itertools.combinations(xs, i) for i in range(len(xs)+1)
               ))
               )


names = list(map(chr, range(ord('a'), ord('z')+1)))


def mk_vars(atomics, nonatomics):
    return mk_set(*(
        f'{p}({f(n)})'
        for f, p, c in ((mk_atomic_name, "AtomicVar", atomics),
                        (mk_nonatomic_name, "NonAtomicVar", nonatomics))
        for n in names[:c]
    ))


def mk_rules(atomics, nonatomics):
    def dur():
        return random.randint(1, 5)

    ns = list(itertools.chain(*(
        map(f, names[:c])
        for f, c in ((mk_atomic_name, atomics),
                     (mk_nonatomic_name, nonatomics))
    )))
    return '        ' + ',\n        '.join((mk_rule(reads, writes, dur())
                                            for writes in subsets(ns)
                                            for reads in subsets(ns)
                                            ))


counts = 2, 1  # atomic, non-atomic variables
print('----------------------------------------------------------------------')
print(mk_rules(*counts))
print('----------------------------------------------------------------------')
print(mk_vars(*counts))
