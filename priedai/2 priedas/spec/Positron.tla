---- MODULE Positron ----

LOCAL INSTANCE Naturals
LOCAL INSTANCE Maybe
LOCAL INSTANCE Sequences

-----------------------------------------------------------------------------
\* Parameters

CONSTANT variables  \* sequence of Atom variables (name, atomicity)
CONSTANT ruleset  \* sequence of Atom rules (varReads, varWrites, duration)

CONSTANT interrupts \* set of interrupts (varReads, varWrites, duration)
CONSTANT minTimeBetweenInterrupts  \* min time to not raise a new interrupt

CONSTANT disableInterrupts  \* disable all interrupts
CONSTANT blockIntsInNonatomicRead  \* block interrupts when non-atomically reading
CONSTANT blockIntsInReadPhase  \* block interrupts for entire read phase
CONSTANT blockIntsInNonatomicWrite  \* block interrupts when non-atomically writing
CONSTANT blockIntsInWritePhase  \* block interrupts for entire write phase
CONSTANT blockIfBothWrite  \* block interrupt if both rule and int write to same vars
CONSTANT blockIfIntReadsWritten  \* block interrupt if int reads what rule writes
CONSTANT blockIfIntWritesRead  \* block interrupt if int writes what rule reads

-----------------------------------------------------------------------------
\* Variables

VARIABLE ruleIdx  \* index of current Atom rule to execute
VARIABLE phase  \* current Atom program execution phase

VARIABLE intIdx  \* index of current interrupt to execute
VARIABLE intPhase  \* current interrupt execution phase

VARIABLE varsToRead  \* sequence of vars to read in current phase
VARIABLE readingVar  \* current non-atomic variable name being read
VARIABLE varsToWrite  \* sequence of vars to write in current phase
VARIABLE writingVar  \* current non-atomic variable name being written

VARIABLE inInterrupt  \* whether we're currently in an interrupt
VARIABLE intVarsToRead  \* sequence of vars to read in current interrupt
VARIABLE intVarsToWrite  \* sequence of vars to write in current interrupt

VARIABLE timeInCycle
VARIABLE timeSinceInterrupt

-----------------------------------------------------------------------------
\* Utilities

atomVariables == <<
    ruleIdx,
    phase,
    varsToRead,
    readingVar,
    varsToWrite,
    writingVar
    >>

interruptVariables == <<
    inInterrupt,
    intIdx,
    intPhase,
    inInterrupt,
    intVarsToRead,
    intVarsToWrite
    >>

timeVariables == <<
    timeInCycle,
    timeSinceInterrupt
    >>

allModelVariables == <<
    atomVariables,
    interruptVariables,
    timeVariables
    >>

Empty(seq) == Len(seq) = 0
NonEmpty(seq) == Len(seq) > 0
OneOrNone(bools) ==
    LET id(x) == x
    IN Len(SelectSeq(bools, id)) <= 1
SeqToSet(seq) == UNION { { seq[i] }: i \in DOMAIN seq }

Cons(x, xs) == <<x>> \o xs

RECURSIVE Foldr(_, _, _)
Foldr(f(_, _), a, xs) ==
    IF xs = <<>> THEN a
    ELSE f(Head(xs), Foldr(f, a, Tail(xs)))

Sum(xs) ==
    LET add(a, b) == a + b
    IN Foldr(add, 0, xs)

Map(f(_), xs) ==
    LET consMap(a, ys) == Cons(f(a), ys)
    IN Foldr(consMap, <<>>, xs)

Max(xs) ==
    LET y == Head(xs)
        ys == Tail(xs)
        max(a, b) == IF a >= b THEN a ELSE b
    IN Foldr(max, y, ys)

ATOMIC == "atomic"
NONATOMIC == "non-atomic"
ATOMICITY == { ATOMIC, NONATOMIC }
AtomicVar(name) == << name, ATOMIC >>
NonAtomicVar(name) == << name, NONATOMIC >>

PassCycleTime(dtime) ==
    /\ timeInCycle' = timeInCycle + dtime

PassTimeSinceInterrupt(dtime) ==
    /\ timeSinceInterrupt' = timeSinceInterrupt + dtime

PassTime(dtime) ==
    /\ PassCycleTime(dtime)
    /\ PassTimeSinceInterrupt(dtime)

varName(var) == var[1]
varAtomicity(var) == var[2]

varNames == { varName(v): v \in variables }

Rule(reads, writes, duration) == << reads, writes, duration >>
Rules == Seq(varNames) \X Seq(varNames) \X {x \in Nat: x > 0}

IsVarAtomic(name) ==
    varAtomicity(CHOOSE v \in variables: varName(v) = name) = ATOMIC


Phases == {
    "start rule",
    "read",
    "finish read",
    "middle",
    "write",
    "finish write",
    "finish rule",
    "end cycle"
    }

InterruptPhases == {
    "read",
    "write",
    "end"
    }

GetReads(rule) == rule[1]
GetWrites(rule) == rule[2]
GetDuration(rule) == rule[3]

RuleVars(rule) == SeqToSet(GetReads(rule) \o GetWrites(rule))

MutuallyAtomic(rule, int) ==
    (RuleVars(rule) \intersect RuleVars(int)) = {}

currentRule == ruleset[ruleIdx]
currentInt == interrupts[intIdx]

isLastRule == ruleIdx = Len(ruleset)

totalRuleDuration == Sum(Map(GetDuration, ruleset))
maxInterruptDuration == Max(<< 0 >> \o Map(GetDuration, interrupts))

worstCaseCycleDuration == (
    totalRuleDuration
    + (totalRuleDuration \div minTimeBetweenInterrupts) * maxInterruptDuration
    )

-----------------------------------------------------------------------------
\* Parameter requirements

\* we have some rules to execute
ASSUME Len(ruleset) > 0

\* input integrity
ASSUME \A v \in variables: v \in STRING \X ATOMICITY

\* all used names are defined
ASSUME ruleset \in Seq(Rules)
ASSUME interrupts \in Seq(Rules)

\* time parameters are non-negative
ASSUME minTimeBetweenInterrupts \in Nat

\* Properties

TypeOK ==
    /\ ruleIdx \in ( 1 .. Len(ruleset) )
    /\ phase \in Phases
    /\ varsToRead \in Seq(varNames)
    /\ readingVar \in Maybe(varNames)
    /\ varsToWrite \in Seq(varNames)
    /\ writingVar \in Maybe(varNames)
    /\ OneOrNone(<<
        Len(varsToRead) > 0,
        Len(varsToWrite) > 0
        >>)
    /\ OneOrNone(<<
        IsJust(readingVar),
        IsJust(writingVar)
        >>)
    /\ IsJust(readingVar) => phase = "finish read"
    /\ IsJust(writingVar) => phase = "finish write"
    \*
    /\ inInterrupt \in BOOLEAN
    /\ Len(interrupts) > 0 => intIdx \in ( 1 .. Len(interrupts) )
    /\ intPhase \in InterruptPhases
    /\ intVarsToRead \in Seq(varNames)
    /\ intVarsToWrite \in Seq(varNames)
    /\ Len(intVarsToRead) > 0 => intPhase = "read"
    /\ Len(intVarsToWrite) > 0 => intPhase = "write"
    \*
    /\ timeInCycle \in Nat
    /\ timeSinceInterrupt \in Nat

-----------------------------------------------------------------------------
\* Steps

StartRule ==
    /\ \neg inInterrupt
    /\ phase = "start rule"
    /\ phase' = "read"
    /\ varsToRead' = GetReads(currentRule) \o GetWrites(currentRule)
    /\ UNCHANGED <<
        interruptVariables,
        timeVariables,
        ruleIdx,
        readingVar,
        varsToWrite,
        writingVar
        >>

PerformRead ==
    /\ \neg inInterrupt
    /\ phase = "read"
    /\
        IF NonEmpty(varsToRead) THEN  \* read the var
            LET nextToRead == Head(varsToRead)
            IN
                /\
                    IF IsVarAtomic(nextToRead) THEN  \* go on to next
                        /\ UNCHANGED phase
                        /\ readingVar' = None
                    ELSE
                        /\ phase' = "finish read"
                        /\ readingVar' = Just(nextToRead)
                /\ varsToRead' = Tail(varsToRead)
        ELSE  \* done reading
            /\ phase' = "middle"
            /\ UNCHANGED << readingVar, varsToRead >>
    /\ UNCHANGED <<
        interruptVariables,
        timeVariables,
        ruleIdx,
        varsToWrite,
        writingVar
        >>

FinishReadIfNotAtomic ==
    /\ \neg inInterrupt
    /\ phase = "finish read"
    /\ phase' = "read"
    /\ readingVar' = None
    /\ UNCHANGED <<
        interruptVariables,
        timeVariables,
        ruleIdx,
        varsToRead,
        varsToWrite,
        writingVar
        >>

Middle ==
    /\ \neg inInterrupt
    /\ phase = "middle"
    /\ phase' = "write"
    /\ varsToWrite' = GetWrites(currentRule)
    /\ UNCHANGED <<
        interruptVariables,
        timeVariables,
        ruleIdx,
        varsToRead,
        readingVar,
        writingVar
        >>

PerformWrite ==
    /\ \neg inInterrupt
    /\ phase = "write"
    /\
        IF NonEmpty(varsToWrite) THEN  \* write the var
            LET nextToWrite == Head(varsToWrite)
            IN
                /\
                    IF IsVarAtomic(nextToWrite) THEN  \* go on to next
                        /\ UNCHANGED phase
                        /\ writingVar' = None
                    ELSE
                        /\ phase' = "finish write"
                        /\ writingVar' = Just(nextToWrite)
                /\ varsToWrite' = Tail(varsToWrite)
        ELSE  \* done writing, start next rule
            /\ phase' = "finish rule"
            /\ UNCHANGED << varsToWrite, writingVar >>
    /\ UNCHANGED <<
        interruptVariables,
        timeVariables,
        ruleIdx,
        readingVar,
        varsToRead
        >>


FinishWriteIfNotAtomic ==
    /\ \neg inInterrupt
    /\ phase = "finish write"
    /\ phase' = "write"
    /\ writingVar' = None
    /\ UNCHANGED <<
        interruptVariables,
        timeVariables,
        ruleIdx,
        readingVar,
        varsToRead,
        varsToWrite
        >>


FinishRule ==
    /\ \neg inInterrupt
    /\ phase = "finish rule"
    /\ PassTime(GetDuration(currentRule))
    /\
        IF isLastRule THEN
            /\ phase' = "end cycle"
            /\ UNCHANGED ruleIdx
        ELSE
            /\ phase' = "start rule"
            /\ ruleIdx' = ruleIdx + 1
    /\ UNCHANGED <<
        interruptVariables,
        readingVar,
        varsToRead,
        varsToWrite,
        writingVar
        >>

-----------------------------------------------------------------------------
\* Interrupt steps
EnterInterrupt ==
    /\ inInterrupt = FALSE
    /\ inInterrupt' = TRUE
    /\ intIdx' \in ( 1 .. Len(interrupts) )
    /\ \neg disableInterrupts
    /\ timeSinceInterrupt >= minTimeBetweenInterrupts
    /\ blockIntsInNonatomicRead
        => phase /= "finish read"
    /\ blockIntsInNonatomicWrite
        => phase /= "finish write"
    /\ blockIntsInReadPhase
        => \neg (phase \in { "read", "finish read" })
    /\ blockIntsInWritePhase
        => \neg (phase \in { "write", "finish write" })
    /\ blockIfBothWrite
        => (SeqToSet(GetWrites(currentRule))
            \intersect SeqToSet(GetWrites(interrupts[intIdx']))) = {}
    /\ blockIfIntReadsWritten
        => (SeqToSet(GetWrites(currentRule))
            \intersect SeqToSet(GetReads(interrupts[intIdx']))) = {}
    /\ blockIfIntWritesRead
        => (SeqToSet(GetReads(currentRule))
            \intersect  SeqToSet(GetWrites(interrupts[intIdx']))) = {}
    /\ intVarsToRead' = GetReads(interrupts[intIdx'])
    /\ intPhase' = "read"
    /\ UNCHANGED <<
        atomVariables,
        timeVariables,
        intVarsToWrite
        >>

PerformReadInInterrupt ==
    /\ inInterrupt
    /\ intPhase = "read"
    /\
        IF NonEmpty(intVarsToRead) THEN  \* read the var
            /\ intVarsToRead' = Tail(intVarsToRead)
            /\ UNCHANGED << intPhase, intVarsToWrite >>
        ELSE  \* done reading, start writing
            /\ intPhase' = "write"
            /\ intVarsToWrite' = GetWrites(currentInt)
            /\ UNCHANGED intVarsToRead
    /\ UNCHANGED <<
        atomVariables,
        timeVariables,
        inInterrupt,
        intIdx
        >>

PerformWriteInInterrupt ==
    /\ inInterrupt
    /\ intPhase = "write"
    /\
        IF NonEmpty(intVarsToWrite) THEN  \* read the var
            /\ intVarsToWrite' = Tail(intVarsToWrite)
            /\ UNCHANGED << intPhase >>
        ELSE  \* done reading, start writing
            /\ intPhase' = "end"
            /\ UNCHANGED << intVarsToWrite >>
    /\ UNCHANGED <<
        atomVariables,
        timeVariables,
        inInterrupt,
        intIdx,
        intVarsToRead
        >>

ExitInterrupt ==
    /\ inInterrupt = TRUE
    /\ inInterrupt' = FALSE
    /\ intPhase = "end"
    /\ timeSinceInterrupt' = 0
    /\ PassCycleTime(GetDuration(currentInt))
    /\ UNCHANGED <<
        atomVariables,
        intIdx,
        intPhase,
        intVarsToRead,
        intVarsToWrite
        >>

-----------------------------------------------------------------------------

EndCycle ==
    /\ phase = "end cycle"
    /\ UNCHANGED allModelVariables

Init ==
    /\ ruleIdx = 1
    /\ phase = "start rule"
    /\ varsToRead = <<>>
    /\ readingVar = None
    /\ varsToWrite = <<>>
    /\ writingVar = None
    \*
    /\ inInterrupt = FALSE
    /\ intIdx = 1
    /\ intPhase = "read"
    /\ intVarsToRead = <<>>
    /\ intVarsToWrite = <<>>
    \*
    /\ timeInCycle = 0
    /\ timeSinceInterrupt = 0

Next ==
    \/ EnterInterrupt
    \/ PerformReadInInterrupt
    \/ PerformWriteInInterrupt
    \/ ExitInterrupt
    \*
    \/ StartRule
    \/ PerformRead
    \/ FinishReadIfNotAtomic
    \/ Middle
    \/ PerformWrite
    \/ FinishWriteIfNotAtomic
    \/ FinishRule
    \*
    \/ EndCycle

Spec ==
    /\ Init
    /\ [][Next]_<<timeInCycle, allModelVariables>>

------------------------------------------------------------------------------

HardRT == \* exists an upper limit
    timeInCycle <= worstCaseCycleDuration

NoTornWrite == \* interrupt writes while rule is writing
    IsJust(writingVar) /\ NonEmpty(intVarsToWrite)
        => GetJust(writingVar) /= Head(intVarsToWrite)

NoTornRead ==
    \* interrupt writes while rule is reading
    /\ (IsJust(readingVar) /\ NonEmpty(intVarsToWrite))
        => GetJust(readingVar) /= Head(intVarsToWrite)
    \* interrupt reads while rule is writing
    /\ (IsJust(writingVar) /\ NonEmpty(intVarsToRead))
        => GetJust(writingVar) /= Head(intVarsToRead)

NoTornMultiWrite == \* interrupt writes vars that rule is/will be writing
    (SeqToSet(GetWrites(currentRule)) \intersect SeqToSet(intVarsToWrite)) = {}

NoTornMultiRead ==
    \* interrupt writes vars that rule is reading / has read
    /\ (SeqToSet(GetReads(currentRule))
        \intersect SeqToSet(intVarsToWrite)) = {}
    \* interrupt reads vars that rule is writing / has written
    /\ (SeqToSet(GetWrites(currentRule))
        \intersect SeqToSet(intVarsToRead)) = {}

==============================================================================

\* sample config

ruleset ==
    <<
        Rule(<<  >>, <<  >>, 3),
        Rule(<< "a_a" >>, <<  >>, 5),
        Rule(<< "a_b" >>, <<  >>, 1),
        Rule(<< "n_a" >>, <<  >>, 1),
        Rule(<< "a_b", "a_a" >>, <<  >>, 2),
        Rule(<< "a_a", "n_a" >>, <<  >>, 3),
        Rule(<< "a_b", "n_a" >>, <<  >>, 3),
        Rule(<< "a_b", "a_a", "n_a" >>, <<  >>, 2),
        Rule(<<  >>, << "a_a" >>, 3),
        Rule(<< "a_a" >>, << "a_a" >>, 4),
        Rule(<< "a_b" >>, << "a_a" >>, 3),
        Rule(<< "n_a" >>, << "a_a" >>, 4),
        Rule(<< "a_b", "a_a" >>, << "a_a" >>, 2),
        Rule(<< "a_a", "n_a" >>, << "a_a" >>, 1),
        Rule(<< "a_b", "n_a" >>, << "a_a" >>, 5),
        Rule(<< "a_b", "a_a", "n_a" >>, << "a_a" >>, 2),
        Rule(<<  >>, << "a_b" >>, 5),
        Rule(<< "a_a" >>, << "a_b" >>, 3),
        Rule(<< "a_b" >>, << "a_b" >>, 1),
        Rule(<< "n_a" >>, << "a_b" >>, 4),
        Rule(<< "a_b", "a_a" >>, << "a_b" >>, 2),
        Rule(<< "a_a", "n_a" >>, << "a_b" >>, 1),
        Rule(<< "a_b", "n_a" >>, << "a_b" >>, 1),
        Rule(<< "a_b", "a_a", "n_a" >>, << "a_b" >>, 2),
        Rule(<<  >>, << "n_a" >>, 3),
        Rule(<< "a_a" >>, << "n_a" >>, 5),
        Rule(<< "a_b" >>, << "n_a" >>, 1),
        Rule(<< "n_a" >>, << "n_a" >>, 4),
        Rule(<< "a_b", "a_a" >>, << "n_a" >>, 3),
        Rule(<< "a_a", "n_a" >>, << "n_a" >>, 5),
        Rule(<< "a_b", "n_a" >>, << "n_a" >>, 4),
        Rule(<< "a_b", "a_a", "n_a" >>, << "n_a" >>, 2),
        Rule(<<  >>, << "a_b", "a_a" >>, 3),
        Rule(<< "a_a" >>, << "a_b", "a_a" >>, 2),
        Rule(<< "a_b" >>, << "a_b", "a_a" >>, 5),
        Rule(<< "n_a" >>, << "a_b", "a_a" >>, 1),
        Rule(<< "a_b", "a_a" >>, << "a_b", "a_a" >>, 4),
        Rule(<< "a_a", "n_a" >>, << "a_b", "a_a" >>, 3),
        Rule(<< "a_b", "n_a" >>, << "a_b", "a_a" >>, 4),
        Rule(<< "a_b", "a_a", "n_a" >>, << "a_b", "a_a" >>, 1),
        Rule(<<  >>, << "a_a", "n_a" >>, 1),
        Rule(<< "a_a" >>, << "a_a", "n_a" >>, 5),
        Rule(<< "a_b" >>, << "a_a", "n_a" >>, 3),
        Rule(<< "n_a" >>, << "a_a", "n_a" >>, 3),
        Rule(<< "a_b", "a_a" >>, << "a_a", "n_a" >>, 1),
        Rule(<< "a_a", "n_a" >>, << "a_a", "n_a" >>, 4),
        Rule(<< "a_b", "n_a" >>, << "a_a", "n_a" >>, 4),
        Rule(<< "a_b", "a_a", "n_a" >>, << "a_a", "n_a" >>, 4),
        Rule(<<  >>, << "a_b", "n_a" >>, 2),
        Rule(<< "a_a" >>, << "a_b", "n_a" >>, 3),
        Rule(<< "a_b" >>, << "a_b", "n_a" >>, 4),
        Rule(<< "n_a" >>, << "a_b", "n_a" >>, 3),
        Rule(<< "a_b", "a_a" >>, << "a_b", "n_a" >>, 3),
        Rule(<< "a_a", "n_a" >>, << "a_b", "n_a" >>, 2),
        Rule(<< "a_b", "n_a" >>, << "a_b", "n_a" >>, 1),
        Rule(<< "a_b", "a_a", "n_a" >>, << "a_b", "n_a" >>, 5),
        Rule(<<  >>, << "a_b", "a_a", "n_a" >>, 4),
        Rule(<< "a_a" >>, << "a_b", "a_a", "n_a" >>, 2),
        Rule(<< "a_b" >>, << "a_b", "a_a", "n_a" >>, 3),
        Rule(<< "n_a" >>, << "a_b", "a_a", "n_a" >>, 1),
        Rule(<< "a_b", "a_a" >>, << "a_b", "a_a", "n_a" >>, 5),
        Rule(<< "a_a", "n_a" >>, << "a_b", "a_a", "n_a" >>, 5),
        Rule(<< "a_b", "n_a" >>, << "a_b", "a_a", "n_a" >>, 3),
        Rule(<< "a_b", "a_a", "n_a" >>, << "a_b", "a_a", "n_a" >>, 4)
    >>

variables ==
    { AtomicVar("a_a"), AtomicVar("a_b"), NonAtomicVar("n_a") }

interrupts ==
    <<
        Rule(<<  >>, <<  >>, 3),
        Rule(<< "a_a" >>, <<  >>, 5),
        Rule(<< "a_b" >>, <<  >>, 1),
        Rule(<< "n_a" >>, <<  >>, 1),
        Rule(<< "a_b", "a_a" >>, <<  >>, 2),
        Rule(<< "a_a", "n_a" >>, <<  >>, 3),
        Rule(<< "a_b", "n_a" >>, <<  >>, 3),
        Rule(<< "a_b", "a_a", "n_a" >>, <<  >>, 2),
        Rule(<<  >>, << "a_a" >>, 3),
        Rule(<< "a_a" >>, << "a_a" >>, 4),
        Rule(<< "a_b" >>, << "a_a" >>, 3),
        Rule(<< "n_a" >>, << "a_a" >>, 4),
        Rule(<< "a_b", "a_a" >>, << "a_a" >>, 2),
        Rule(<< "a_a", "n_a" >>, << "a_a" >>, 1),
        Rule(<< "a_b", "n_a" >>, << "a_a" >>, 5),
        Rule(<< "a_b", "a_a", "n_a" >>, << "a_a" >>, 2),
        Rule(<<  >>, << "a_b" >>, 5),
        Rule(<< "a_a" >>, << "a_b" >>, 3),
        Rule(<< "a_b" >>, << "a_b" >>, 1),
        Rule(<< "n_a" >>, << "a_b" >>, 4),
        Rule(<< "a_b", "a_a" >>, << "a_b" >>, 2),
        Rule(<< "a_a", "n_a" >>, << "a_b" >>, 1),
        Rule(<< "a_b", "n_a" >>, << "a_b" >>, 1),
        Rule(<< "a_b", "a_a", "n_a" >>, << "a_b" >>, 2),
        Rule(<<  >>, << "n_a" >>, 3),
        Rule(<< "a_a" >>, << "n_a" >>, 5),
        Rule(<< "a_b" >>, << "n_a" >>, 1),
        Rule(<< "n_a" >>, << "n_a" >>, 4),
        Rule(<< "a_b", "a_a" >>, << "n_a" >>, 3),
        Rule(<< "a_a", "n_a" >>, << "n_a" >>, 5),
        Rule(<< "a_b", "n_a" >>, << "n_a" >>, 4),
        Rule(<< "a_b", "a_a", "n_a" >>, << "n_a" >>, 2),
        Rule(<<  >>, << "a_b", "a_a" >>, 3),
        Rule(<< "a_a" >>, << "a_b", "a_a" >>, 2),
        Rule(<< "a_b" >>, << "a_b", "a_a" >>, 5),
        Rule(<< "n_a" >>, << "a_b", "a_a" >>, 1),
        Rule(<< "a_b", "a_a" >>, << "a_b", "a_a" >>, 4),
        Rule(<< "a_a", "n_a" >>, << "a_b", "a_a" >>, 3),
        Rule(<< "a_b", "n_a" >>, << "a_b", "a_a" >>, 4),
        Rule(<< "a_b", "a_a", "n_a" >>, << "a_b", "a_a" >>, 1),
        Rule(<<  >>, << "a_a", "n_a" >>, 1),
        Rule(<< "a_a" >>, << "a_a", "n_a" >>, 5),
        Rule(<< "a_b" >>, << "a_a", "n_a" >>, 3),
        Rule(<< "n_a" >>, << "a_a", "n_a" >>, 3),
        Rule(<< "a_b", "a_a" >>, << "a_a", "n_a" >>, 1),
        Rule(<< "a_a", "n_a" >>, << "a_a", "n_a" >>, 4),
        Rule(<< "a_b", "n_a" >>, << "a_a", "n_a" >>, 4),
        Rule(<< "a_b", "a_a", "n_a" >>, << "a_a", "n_a" >>, 4),
        Rule(<<  >>, << "a_b", "n_a" >>, 2),
        Rule(<< "a_a" >>, << "a_b", "n_a" >>, 3),
        Rule(<< "a_b" >>, << "a_b", "n_a" >>, 4),
        Rule(<< "n_a" >>, << "a_b", "n_a" >>, 3),
        Rule(<< "a_b", "a_a" >>, << "a_b", "n_a" >>, 3),
        Rule(<< "a_a", "n_a" >>, << "a_b", "n_a" >>, 2),
        Rule(<< "a_b", "n_a" >>, << "a_b", "n_a" >>, 1),
        Rule(<< "a_b", "a_a", "n_a" >>, << "a_b", "n_a" >>, 5),
        Rule(<<  >>, << "a_b", "a_a", "n_a" >>, 4),
        Rule(<< "a_a" >>, << "a_b", "a_a", "n_a" >>, 2),
        Rule(<< "a_b" >>, << "a_b", "a_a", "n_a" >>, 3),
        Rule(<< "n_a" >>, << "a_b", "a_a", "n_a" >>, 1),
        Rule(<< "a_b", "a_a" >>, << "a_b", "a_a", "n_a" >>, 5),
        Rule(<< "a_a", "n_a" >>, << "a_b", "a_a", "n_a" >>, 5),
        Rule(<< "a_b", "n_a" >>, << "a_b", "a_a", "n_a" >>, 3),
        Rule(<< "a_b", "a_a", "n_a" >>, << "a_b", "a_a", "n_a" >>, 4)
    >>
