{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleContexts #-}
module Language.Positron
    ( Positron
    , FuncRef
    , VarRef
    , getVarName
    , needHeaders
    , function
    , interrupt
    , simpleInterrupt
    , global
    , global'
    , globalArr
    , globalArr'
    , volatileGlobal
    , volatileGlobal'
    , volatileGlobalArr
    , volatileGlobalArr'
    , defMain
    , addSetup
    , addSetup'
    , apply
    , apply'
    , liftAtom
    , positron
    , compile
    )
where

import           Control.Lens
import           Data.List                      ( intercalate
                                                , nub
                                                )
import           Data.Maybe                     ( isNothing )
import           Control.Monad.IO.Class
import           Control.Monad.State
import qualified Data.Map.Strict               as M

import qualified Language.Atom                 as A


data PositronSt = PositronSt
    { _path :: String
    , _functions :: M.Map Name Func
    , _interrupts :: M.Map ISRVector ISR
    , _globals :: M.Map Name Global
    , _includes :: [Header]
    , _main :: Maybe Main
    , _inits :: [(FuncRef, [String])]
    , _subAtom :: Maybe (A.Atom ())
    }

instance Show PositronSt where
    show PositronSt{..} = concat
        [ "PositronSt { _path="
        , show _path
        , ", _functions=("
        , show _functions
        , "), _interrupts=("
        , show _interrupts
        , "), _globals=("
        , show _globals
        , "), _includes=("
        , show _includes
        , "), _main="
        , case _main of
            Nothing -> show (Nothing :: Maybe Int)
            _ -> "Just _"
        , "), _inits="
        , show $ map (\(FuncRef n _, _) -> n) _inits
        , ", _subAtom=_"
        ]

emptyPositronSt :: PositronSt
emptyPositronSt = PositronSt
    { _path       = ""
    , _functions  = M.empty
    , _interrupts = M.empty
    , _globals    = M.empty
    , _includes   = []
    , _main       = Nothing
    , _inits      = []
    , _subAtom    = Nothing
    }

type Name = String
type Arg = String
type Body = [String]
type ISRVector = String
type ISRAttribute = String
type Decl = String
type Header = String
type Main = Name -> Positron Body


newtype Positron a = Positron
    { runPositron :: StateT PositronSt IO a
    } deriving (Functor, Applicative, Monad)

liftS :: StateT PositronSt IO a -> Positron a
liftS = Positron


data Func = Func
    { arguments :: [Arg]
    , fBody :: Body
    } deriving (Show, Eq)

data ISR = ISR
    { vector :: ISRVector
    , attributes :: [ISRAttribute]
    , iBody :: Body
    } deriving (Show, Eq)

data Global = Global
    { gType     :: A.Type
    , gLength   :: Maybe Int
    , gValue    :: Maybe String
    , gVolatile :: Volatility
    } deriving (Show, Eq)


data Volatility = NonVolatile | Volatile deriving (Show, Eq)


data FuncRef = FuncRef Name ([String] -> String)
newtype VarRef = VarRef Name

makeLenses ''PositronSt  -- need PositronSt's contents above

getVarName :: VarRef -> Name
getVarName (VarRef n) = n


apply :: FuncRef -> [A.UE] -> A.Atom ()
apply = A.action . apply'


apply' :: FuncRef -> [String] -> String
apply' (FuncRef _ f) = f


makeCall :: Name -> [String] -> String
makeCall name vals = concat [name, "(", intercalate ", " vals, ")"]


needHeaders :: [Header] -> Positron ()
needHeaders incs' = liftS $ (includes %=) $ nub . (++ incs')


function :: Name -> [Arg] -> Body -> Positron FuncRef
function name args body = do
    liftS $ functions %= M.insertWith (alreadyDefined "Function" name)
                                      name
                                      (Func args body)
    pure $ FuncRef name $ makeCall name


defMain :: (Name -> Positron Body) -> Positron ()
defMain bodygen = liftS $ main .= Just bodygen


addSetup :: FuncRef -> [String] -> Positron ()
addSetup fr args = liftS $ inits %= (++ [(fr, args)])


addSetup' :: FuncRef -> Positron ()
addSetup' fr = addSetup fr []


interrupt :: ISRVector -> [ISRAttribute] -> Body -> Positron ()
interrupt isr attrs body = liftS $ interrupts %= M.insertWith
    (alreadyDefined "ISR" isr)
    isr
    (ISR isr attrs body)


simpleInterrupt :: ISRVector -> Body -> Positron ()
simpleInterrupt isr = interrupt isr []


global :: A.Type -> Name -> String -> Positron VarRef
global t name initVal = global_ t name Nothing NonVolatile $ Just initVal


global' :: A.Type -> Name -> Positron VarRef
global' t name = global_ t name Nothing NonVolatile Nothing


globalArr :: A.Type -> Name -> [String] -> Positron VarRef
globalArr t name vals =
    let val = concat ["{ ", intercalate ", " vals, " }"]
    in  global_ t name (Just $ length vals) NonVolatile $ Just val


globalArr' :: A.Type -> Name -> Int -> Positron VarRef
globalArr' t name size = global_ t name (Just size) NonVolatile Nothing


volatileGlobal :: A.Type -> Name -> String -> Positron VarRef
volatileGlobal t name initVal = global_ t name Nothing Volatile $ Just initVal


volatileGlobal' :: A.Type -> Name -> Positron VarRef
volatileGlobal' t name = global_ t name Nothing Volatile Nothing


volatileGlobalArr :: A.Type -> Name -> [String] -> Positron VarRef
volatileGlobalArr t name vals =
    let val = concat ["{ ", intercalate ", " vals, " }"]
    in  global_ t name (Just $ length vals) Volatile $ Just val


volatileGlobalArr' :: A.Type -> Name -> Int -> Positron VarRef
volatileGlobalArr' t name size = global_ t name (Just size) Volatile Nothing


global_
    :: A.Type
    -> Name
    -> Maybe Int
    -> Volatility
    -> Maybe String
    -> Positron VarRef
global_ t name mlength vol val = do
    liftS $ globals %= M.insertWith (alreadyDefined "Global" name)
                                    name
                                    (Global t mlength val vol)
    pure $ VarRef name




liftAtom :: A.Atom () -> Positron ()
liftAtom x = liftS $ subAtom %= (`maybeSeq` Just x)


positron :: String -> Positron () -> Positron ()
positron name sub = liftS $ do
    subSt <- lift $ collapseSt name . snd <$> buildPositron sub
    functions %= M.unionWithKey (alreadyDefined "Function") (subSt ^. functions)
    interrupts %= M.unionWithKey (alreadyDefined "ISR") (subSt ^. interrupts)
    includes %= nub . (++ subSt ^. includes)
    subAtom %= maybeSeq (subSt ^. subAtom)


prependKeys
    :: (Ord a, Show a, Show b)
    => [a]
    -> Getting (M.Map [a] b) s (M.Map [a] b)
    -> s
    -> M.Map [a] b
prependKeys p l = M.mapKeys (p ++) . (^. l)


collapseSt :: String -> PositronSt -> PositronSt
collapseSt name st =
    let relPath = case st ^. path of
            "" -> ""
            p  -> p ++ "."
        subf = case st ^. path of
            "" -> st ^. subAtom
            p  -> A.atom p <$> st ^. subAtom
    in  st
        & (path .~ name)
        . (functions .~ prependKeys relPath functions st)
        . (globals .~ prependKeys relPath globals st)
        . (subAtom .~ subf)


buildPositron :: Positron a -> IO (a, PositronSt)
buildPositron pos = runStateT (runPositron pos) emptyPositronSt


compile :: String -> Positron () -> IO (Maybe A.Schedule)
compile name pos = do
    let atomName       = name ++ "_atom"
        atomHeader     = atomName ++ ".h"
        positronName   = name ++ "_positron"
        positronHeader = positronName ++ ".h"
        positronImpl   = positronName ++ ".c"
        withMain       = do
            pos
            needHeaders [atomHeader]
            noMain <- isNothing <$> liftS (use main)
            when noMain $ defMain $ \step ->
                pure [concat ["for (;;) { ", step, "(); }"]]
        withDefMain = do
            withMain
            Just fmain <- liftS $ use main
            fmain name

    (mainBody, posSt) <- fmap (collapseSt "") <$> buildPositron withDefMain
    let funcs      = M.toAscList $ posSt ^. functions
        ints       = M.elems $ posSt ^. interrupts
        globs      = M.toAscList $ posSt ^. globals
        incs       = posSt ^. includes ++ [positronHeader]
        setups     = map (\(FuncRef _ f, as) -> f as ++ ";") $ posSt ^. inits
        header     = unlines $ concat
            [ ["/* Generated by Positron */", ""]
            , map genInclude ["stdint.h", "stdbool.h"]
            , ["", "/* Globals */", ""]
            , map (uncurry genGlobalDecl) globs
            , ["", "/* Function declarations */", ""]
            , map (uncurry genFunctionFwd) funcs
            ]
        implementation = unlines $ concat
            [ ["/* Generated by Positron */", ""]
            , map genInclude incs
            , ["", "/* Globals */", ""]
            , map (uncurry genGlobal) globs
            , ["", "/* Functions */", ""]
            , map (uncurry genFunction) funcs
            , ["", "/* ISRs */", ""]
            , map genISR ints
            , ["", "/* Main */", ""]
            , [ genProc "void _positron_main()" mainBody
              , genProc "int main()" $ setups ++ ["_positron_main();"]
              ]
            ]
    liftIO $ do
        writeFile positronHeader header
        writeFile positronImpl   implementation
    let atomCfg = A.defaults
            { A.cFuncName     = name
            , A.cCode = \_ _ _ -> ("#include \"" ++ name ++ "_positron.h\"", "")
            , A.cRuleCoverage = False
            , A.cAssert       = False
            }
    mAtomStuff <-
        liftIO
        $   sequence
        $   A.compile (name ++ "_atom") atomCfg
        <$> (posSt ^. subAtom)
    pure $ (^. _1) <$> mAtomStuff


genInclude :: Name -> Decl
genInclude header = concat ["#include \"", header, "\""]


genFuncDecl :: Name -> [Arg] -> Decl
genFuncDecl name args' =
    concat ["void ", replaceDots name, "(", intercalate ", " args', ")"]


genISRDecl :: ISR -> Decl
genISRDecl ISR { vector = vect, attributes = attrs } =
    concat
        $  ["ISR(", vect]
        ++ case attrs of
               [] -> []
               _  -> [intercalate ", " attrs]
        ++ [")"]


genGlobalDecl :: Name -> Global -> Decl
genGlobalDecl name Global { gType = t, gLength = ml, gVolatile = vol } =
    concat $ concat
        [ ["extern "]
        , [ "volatile " | vol == Volatile ]
        , [A.cType t, " ", replaceDots name]
        , maybe [] (const ["[]"]) ml
        , [";"]
        ]


genGlobal :: Name -> Global -> Decl
genGlobal name Global { gType = t, gLength = ml, gValue = mv, gVolatile = vol }
    = concat $ concat
        [ [ "volatile " | vol == Volatile ]
        , [A.cType t, " ", replaceDots name]
        , maybe [] (\l -> ["[", show l, "]"]) ml
        , maybe [] (\v -> [" = ", v])         mv
        , [";"]
        ]


replaceDots :: Name -> Name
replaceDots = map $ \c -> case c of
    '.' -> '_'
    _   -> c


genProc :: Decl -> Body -> String
genProc decl body = unlines $ [decl, "{"] ++ map ("    " ++) body ++ ["}", ""]


genFunction :: Name -> Func -> String
genFunction name Func { arguments = args, fBody = body } =
    genProc (genFuncDecl name args) body


genFunctionFwd :: Name -> Func -> String
genFunctionFwd name Func { arguments = args } = genFuncDecl name args ++ ";"


genISR :: ISR -> String
genISR isr = genProc (genISRDecl isr) (iBody isr)


maybeSeq :: (Monad m) => Maybe (m a) -> Maybe (m a) -> Maybe (m a)
maybeSeq (  Just ma) (Just mb)  = Just $ ma >> mb
maybeSeq a@(Just _ ) Nothing    = a
maybeSeq Nothing     b@(Just _) = b
maybeSeq Nothing     Nothing    = Nothing


alreadyDefined :: String -> Name -> a
alreadyDefined t name =
    error $ concat [t, " ", name, " has already been defined in this scope."]
