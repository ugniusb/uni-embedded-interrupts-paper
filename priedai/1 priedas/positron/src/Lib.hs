{-# LANGUAGE RecordWildCards #-}
module Lib
    ( codegen
    )
where

import           Data.Char
import           Data.Word
import           Data.List                      ( intercalate )
import qualified Language.Atom                 as A
import           Language.Positron             as P


flags :: [String] -> String
flags fs = concat
    ["(", intercalate " | " $ map (\s -> concat ["(1 << (", s, "))"]) fs, ")"]


noInterrupts' :: String -> [String] -> [String]
noInterrupts' name xs =
    [concat ["uint8_t ", name, " = SREG;"], "cli();"]
        ++ xs
        ++ [concat ["SREG = ", name, ";"]]

noInterrupts :: [String] -> [String]
noInterrupts = noInterrupts' "sreg"


block :: [String] -> [String]
block ls = concat [["{"], map ("    " ++) ls, ["}"]]


initLoopClock :: Positron VarRef
initLoopClock = do
    needHeaders ["avr/interrupt.h"]

    (addSetup' =<<)
        $ function "initClock" []
        $ noInterrupts
              [
        -- no PWM output, fast PWM with CTC mode (0 .. OCR1A), 8 prescale
                concat ["TCCR1A = ", flags ["WGM11", "WGM10"], ";"]
              , concat ["TCCR1B = ", flags ["WGM13", "WGM12", "CS11"], ";"]
        -- set period
              , "OCR1A = (uint16_t)((uint32_t)F_CPU / 8 / 250) - 1;"
        -- reset clock
              , "TCNT1 = 0;"
        -- enable OVF interrupt
              , concat ["TIMSK1 |= ", flags ["TOIE1"], ";"]
              ]

    cycles <- volatileGlobal A.Word32 "cycles" "0"
    let vCycles = getVarName cycles

    defMain $ \stepFnName -> do
        simpleInterrupt "TIMER1_OVF_vect"
                        [vCycles ++ "++;", "sei();", stepFnName ++ "();"]
        pure ["while (1);"]
    pure cycles


initInterrupts :: Positron ()
initInterrupts = do
    needHeaders ["avr/interrupt.h"]
    addSetup' =<< function "enableInterrupts" [] ["sei();"]


initUART :: Word32 -> Word8 -> Positron FuncRef
initUART baud bufSizeBits = do
    needHeaders ["avr/io.h", "avr/interrupt.h"]
    (addSetup' =<<)
        $ function "initUART" []
        $ noInterrupts
              [ concat ["UBRR0 = F_CPU / 16 / ", show baud, " - 1;"]
        -- enable tx interrupt
              , concat ["UCSR0B |= ", flags ["TXEN0", "TXCIE0"], ";"]
        -- 8 data, 1 stop bits
              , concat ["UCSR0C |= ", flags ["UCSZ00", "UCSZ01"], ";"]
              ]

    let bufSize  = fromIntegral ((2 :: Int) ^ bufSizeBits)
        bufSize' = show bufSize
        idxType | bufSizeBits <= 8 = A.Word8
                | otherwise        = A.Word16
        idxType' = A.cType idxType
    writeBuf <- volatileGlobalArr' A.Word8 "uart_write_buf" bufSize
    readIdx  <- volatileGlobal idxType "uart_write_read_idx" "0"
    writeIdx <- global idxType "uart_write_write_idx" "0"
    uartIdle <- volatileGlobal A.Bool "uart_write_idle" "true"
    do
        let buf  = getVarName writeBuf
            ri   = getVarName readIdx
            wi   = getVarName writeIdx
            idle = getVarName uartIdle
            nextIdx v = concat ["((", v, " + 1) % ", bufSize', ")"]
            condBlock b cond ss = concat [b, " (", cond, ")"] : block ss
            if_ = condBlock "if"
            else_ ss = "else" : block ss
            while_ = condBlock "while"
            waitWhile_ cond = concat ["while (", cond, ");"]
        simpleInterrupt "USART_TX_vect"
            $  concat [idxType', " _ri = ", ri, ";"]
            :  if_
                   ("_ri != " ++ wi)
                   [ concat [ri, " = ", nextIdx "_ri", ";"]
                   , concat ["UDR0 = ", buf, "[_ri];"]
                   ]
            ++ else_ [idle ++ " = true;"]

        writeByte <-
            function "writeUARTByte" ["const uint8_t data"]
            $  if_ idle [idle ++ " = false;", "UDR0 = data;"]
            ++ else_
                   (  [ concat [idxType', " nwi = ", nextIdx wi, ";"]
                      , waitWhile_ ("nwi == " ++ ri)
                      ]
                   ++ noInterrupts
                          [ concat [buf, "[", wi, "] = data;"]
                          , wi ++ " = nwi;"
                          ]
                   )
        function "writeUART" ["const uint8_t* data", "uint8_t length"]
            $ "uint8_t i = 0;"
            : while_ "i < length" [writeByte `apply'` ["data[i++]"] ++ ";"]


mkWriteWord :: Int -> A.Type -> FuncRef -> Positron FuncRef
mkWriteWord numDigits argType writeUart = do
    let argT = A.cType argType
        writeDigit i =
            [concat ["buf[", show i, "] = digits[value % 10];"], "value /= 10;"]
    function ("writeWord_" ++ argT) [argT ++ " value"]
        $  [ "const uint8_t* digits = (const uint8_t*)\"0123456789\";"
           , concat ["uint8_t buf[", show numDigits, "];"]
           ]
        ++ concatMap writeDigit (reverse [0 .. numDigits - 1])
        ++ [writeUart `apply'` ["buf", show numDigits] ++ ";"]


mkWriteStr :: String -> String -> FuncRef -> Positron FuncRef
mkWriteStr name text writeUart =
    let bytes = intercalate ", " $ map (show . ord) text
    in function
        ("writeStr_" ++ name)
        []
        [ concat ["const uint8_t buf[] = { ", bytes, " };"]
        , writeUart `apply'` ["buf", show $ length text] ++ ";"
        ]


initADC :: Positron (VarRef, VarRef, FuncRef)
initADC = do
    needHeaders ["avr/io.h", "avr/interrupt.h"]
    (addSetup' =<<)
        $ function "initADC" []
        $ noInterrupts
              [ "DDRC = 0;"  -- PC has the ADC inputs, so set all as inputs
              , "ADMUX = (1 << REFS0);"
        -- enable ADC, interrupt and clock prescale = 128
              , concat
                  [ "ADCSRA = "
                  , flags ["ADEN", "ADIE", "ADPS0", "ADPS1", "ADPS2"]
                  , ";"
                  ]
              ]
    val      <- volatileGlobal A.Word16 "adc_value" "-1"
    rdy      <- volatileGlobal A.Bool "adc_ready" "false"
    nextChan <- volatileGlobal A.Int8 "adc_next_channel" "-1"
    let vVal      = getVarName val
        vRdy      = getVarName rdy
        vNextChan = getVarName nextChan
    fnReadChan <-
        function "readChanADC" ["const uint8_t chan"]
        $  [ concat ["bool idle = ", vNextChan, " == -1;"]
           , vNextChan ++ " = chan & 0x7;"
           , "if (idle)"
           ]
        ++ block [vRdy ++ " = false;", "ADCSRA |= 1 << ADSC;"]
    simpleInterrupt "ADC_vect"
        $  [ vVal ++ " = ADC;"
           , vRdy ++ " = true;"
           , concat ["if (", vNextChan, " != -1)"]
           ]
        ++ block
               [ concat ["ADMUX = (ADMUX & 0xF8) | (", vNextChan, " & 0x7);"]
               , vNextChan ++ "= -1;"
               , "ADCSRA |= 1 << ADSC;"
               ]
    pure (val, rdy, fnReadChan)


data AtomDeps = AtomDeps
    { adcValVar   :: VarRef
    , adcReadyVar :: VarRef
    , adcRead     :: FuncRef
    , writeWord16 :: FuncRef
    , writeWord32 :: FuncRef
    , writeSpace  :: FuncRef
    , writeNL     :: FuncRef
    , cyclesVar   :: VarRef
    }


atomMain :: Word8 -> Word8 -> AtomDeps -> A.Atom ()
atomMain adcChannel windowSize AtomDeps {..} = do
    prevAdcVals <- A.array "prev_adc_vals"
        $ replicate (fromIntegral windowSize) 0

    A.atom "read_loop" $ do
        idx <- A.word8 "i" 0
        let adcVal = A.value $ A.word16' $ getVarName adcValVar
        (prevAdcVals A.! A.value idx) A.<== adcVal
        idx A.<== (A.value idx + 1) `A.mod_` A.Const windowSize
        adcRead `apply` [A.ue $ A.Const adcChannel]

    A.atom "print_loop" $ do
        prevMA <- A.word16 "prev_moving_average" 0
        let cycles = A.word32' $ getVarName cyclesVar
        let movingSum =
                sum $ map ((prevAdcVals A.!.) . A.Const) [0 .. windowSize - 1]
        let movingAverage = movingSum `A.div_` A.Cast (A.Const windowSize)
        A.cond $ A.value prevMA A./=. movingAverage
        prevMA A.<== movingAverage
        writeWord32 `apply` [A.ue $ A.value cycles]
        writeSpace `apply` []
        writeWord16 `apply` [A.ue movingAverage]
        writeNL `apply` []


positronMain :: Positron ()
positronMain = do
    let adcChannel = 0
        windowSize = 5
    (adcValVar, adcReadyVar, adcRead) <- initADC
    addSetup adcRead [show adcChannel]
    writeUart   <- initUART 57600 7 -- 128 bytes
    writeWord16 <- mkWriteWord 5 A.Word16 writeUart
    writeWord32 <- mkWriteWord 10 A.Word32 writeUart
    writeSpace  <- mkWriteStr "space" " " writeUart
    writeNL     <- mkWriteStr "nl" "\r\n" writeUart
    cyclesVar   <- initLoopClock
    liftAtom $ atomMain adcChannel windowSize AtomDeps {..}
    initInterrupts


codegen :: IO ()
codegen = do
    Just schedule <- P.compile "adc_demo" positronMain
    putStrLn $ A.reportSchedule schedule
