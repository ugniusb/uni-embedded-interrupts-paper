callWithArgs :: String -> [UE] -> Atom ()
callWithArgs fname =
    action (\args -> concat [fname, "(", intercalate ", " args, ")"])


atomMain :: Word8 -> Word8 -> Atom ()
atomMain adcChannel windowSize = do
    prevAdcVals <-
        array "prev_adc_vals" $ replicate (fromIntegral windowSize) 0

    atom "read_loop" $ do
        idx <- word8 "i" 0
        let adcVal = value $ word16' "adc_value"
        (prevAdcVals ! value idx) <== adcVal
        idx <== (value idx + 1) `mod_` Const windowSize
        callWithArgs "readChanADC" [ue $ Const adcChannel]

    atom "print_loop" $ do
        prevMA <- word16 "prev_moving_average" 0
        let vcycles = value $ word32' "cycles"
        let movingSum =
                sum $ map ((prevAdcVals !.) . Const) [0 .. windowSize - 1]
        let movingAverage = movingSum `div_` Cast (Const windowSize)
        cond $ value prevMA /=. movingAverage
        prevMA <== movingAverage
        callWithArgs "writeUint32" [ue vcycles]
        call "writeSpace"
        callWithArgs "writeUint16" [ue movingAverage]
        call "writeNl"
