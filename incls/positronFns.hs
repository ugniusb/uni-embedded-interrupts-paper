type Name         = String
type Arg          = String
type Body         = [String]
type ISRVector    = String
type ISRAttribute = String
type Header       = String

liftAtom    :: Atom.Atom () -> Positron ()
needHeaders :: [Header]     -> Positron ()
getVarName  :: VarRef -> Name

function        :: Name      -> [Arg] ->          Body -> Positron FuncRef
interrupt       :: ISRVector -> [ISRAttribute] -> Body -> Positron ()
simpleInterrupt :: ISRVector ->                   Body -> Positron ()

apply    :: FuncRef -> [Atom.UE] -> Atom.Atom ()
apply'   :: FuncRef -> [String]  -> String

defMain   :: (Name -> Positron Body) -> Positron ()
addSetup  :: FuncRef -> [String] ->     Positron ()
addSetup' :: FuncRef ->                 Positron ()

global             :: Atom.Type -> Name -> String ->   Positron VarRef
global'            :: Atom.Type -> Name ->             Positron VarRef
globalArr          :: Atom.Type -> Name -> [String] -> Positron VarRef
globalArr'         :: Atom.Type -> Name -> Int ->      Positron VarRef
volatileGlobal     :: Atom.Type -> Name -> String ->   Positron VarRef
volatileGlobal'    :: Atom.Type -> Name ->             Positron VarRef
volatileGlobalArr  :: Atom.Type -> Name -> [String] -> Positron VarRef
volatileGlobalArr' :: Atom.Type -> Name -> Int ->      Positron VarRef

