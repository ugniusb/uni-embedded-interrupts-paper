inicializacijaNeatlikta <- bool "inicializacijaNeatlikta" True

atom "inicializacija" $ do
    cond (value inicializacijaNeatlikta)
    inicializacijaNeatlikta <== False
    action $ \_ -> unlines
        [ "cli();" -- specifikacija reikalauja išjungti pertrauktis
        , "ADMUX = (1 << REFS0);"  -- vienas iš ADC registrų
        -- ... visa kita inicializacija
        , "sei();" -- įjungiame pertrauktis
        ]
