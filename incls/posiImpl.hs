data AtomDeps = AtomDeps
    { adcValVar   :: VarRef
    , adcReadyVar :: VarRef
    , adcRead     :: FuncRef
    , writeWord16 :: FuncRef
    , writeWord32 :: FuncRef
    , writeSpace  :: FuncRef
    , writeNL     :: FuncRef
    , cyclesVar   :: VarRef
    }

atomMain :: Word8 -> Word8 -> AtomDeps -> A.Atom ()
atomMain adcChannel windowSize AtomDeps {..} = do
    prevAdcVals <- A.array "prev_adc_vals"
        $ replicate (fromIntegral windowSize) 0

    A.atom "read_loop" $ do
        idx <- A.word8 "i" 0
        let adcVal = A.value $ A.word16' $ getVarName adcValVar
        (prevAdcVals A.! A.value idx) A.<== adcVal
        idx A.<== (A.value idx + 1) `A.mod_` A.Const windowSize
        adcRead `apply` [A.ue $ A.Const adcChannel]

    A.atom "print_loop" $ do
        prevMA <- A.word16 "prev_moving_average" 0
        let cycles = A.word32' $ getVarName cyclesVar
        let movingSum =
                sum $ map ((prevAdcVals A.!.) . A.Const) [0 .. windowSize - 1]
        let movingAverage = movingSum `A.div_` A.Cast (A.Const windowSize)
        A.cond $ A.value prevMA A./=. movingAverage
        prevMA A.<== movingAverage
        writeWord32 `apply` [A.ue $ A.value cycles]
        writeSpace `apply` []
        writeWord16 `apply` [A.ue movingAverage]
        writeNL `apply` []

positronMain :: Positron ()
positronMain = do
    let adcChannel = 0
        windowSize = 5
    (adcValVar, adcReadyVar, adcRead) <- initADC
    addSetup adcRead [show adcChannel]
    writeUart   <- initUART 57600 7 -- 128 bytes
    writeWord16 <- mkWriteWord 5 A.Word16 writeUart
    writeWord32 <- mkWriteWord 10 A.Word32 writeUart
    writeSpace  <- mkWriteStr "space" " " writeUart
    writeNL     <- mkWriteStr "nl" "\r\n" writeUart
    cyclesVar   <- initLoopClock
    liftAtom $ atomMain adcChannel windowSize AtomDeps {..}
    initInterrupts

mkWriteWord :: Int -> A.Type -> FuncRef -> Positron FuncRef
mkWriteWord numDigits argType writeUart = do
    let argT = A.cType argType
        writeDigit i =
            [concat ["buf[", show i, "] = digits[value % 10];"], "value /= 10;"]
    function ("writeWord_" ++ argT) [argT ++ " value"]
        $  [ "const uint8_t* digits = (const uint8_t*)\"0123456789\";"
           , concat ["uint8_t buf[", show numDigits, "];"]
           ]
        ++ concatMap writeDigit (reverse [0 .. numDigits - 1])
        ++ [writeUart `apply'` ["buf", show numDigits] ++ ";"]
