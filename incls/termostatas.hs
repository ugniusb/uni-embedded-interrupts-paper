termostatas :: Float -> Atom ()
termostatas histereze = atom "termostatas" $ do
    let temperatura = value $ float' "temperatura"
        norimaTemp  = value $ float' "norima_temp"
    sildoma <- bool "sildoma" False

    let spausdinti tekstas = action
            (\_ -> concat ["printf(\"", tekstas, "\")"])
            []
        jungtiSildytuva rezimas = action
            (\[v] -> concat ["sildyti(\"", v, "\")"])
            [ue rezimas]
        jungtiAusintuva rezimas = action
            (\[v] -> concat ["ausinti(\"", v, "\")"])
            [ue rezimas]

    atom "sildymas" $ do
        cond $ not_ (value sildoma)
        cond $ temperatura < norimaTemp - histereze
        sildoma <== True
        jungtiSildytuva True
        jungtiAusintuva False
        spausdinti "sildoma"

    atom "ausinimas" $ do
        sildoma <== False
        cond $ temperatura > norimaTemp + histereze
        jungtiSildytuva False
        spausdinti "ausinama"
        jungtiAusintuva True
        cond $ not_ (value ausinama)
